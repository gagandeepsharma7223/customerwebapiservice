﻿using NDashboardWS.API.Models;
using NDashboardWS.API.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NDashboardWS.API.Controllers
{
    public class NDashboardWSController : ApiController
    {
        [Route("NMSDashboardWS/activeEvents")]
        [HttpGet]
        public HttpResponseMessage Get(int pageno, int limit, string filterby = "")
        {
            try
            {
                var result = DbService.GetActiveEvents(pageno, limit);
                var objApiResult = new ApiResult
                {
                    totalCount = result.Count(),
                    totalPages = result.Count() > 0 ? (result.FirstOrDefault().TotalCount / limit < limit ? 1 : (int)Math.Ceiling(Convert.ToDecimal(result.FirstOrDefault().TotalCount) / limit)) : 0,
                    pageMetadata = new PageMetaData
                    {
                        content = result.Cast<object>().ToArray(),
                        count = result.Count(),
                        pageNo = pageno
                    }
                };
                return Request.CreateResponse(HttpStatusCode.OK, objApiResult);
            }
            catch (Exception exception)
            {
                ErrorLog.LogError(exception);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, exception.Message);
            }
        }

        [Route("NMSDashboardWS/activeEvents/summary/{eventId}")]
        [HttpGet]
        public HttpResponseMessage Summary(int eventId)
        {
            try
            {
                Summary result = DbService.GetSummaryByEventId(eventId);
                var objApiResult = new ApiResult
                {
                    totalCount = result != null ? 1 : 0,
                    pageMetadata = new PageMetaData
                    {
                        content = new object[1] { result },
                    }
                };
                return Request.CreateResponse(HttpStatusCode.OK, objApiResult);
            }
            catch (Exception exception)
            {
                ErrorLog.LogError(exception);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, exception.Message);
            }
        }

        [Route("NMSDashboardWS/activeEvents/notes/{incidentId}")]
        [HttpGet]
        public HttpResponseMessage Notes(int incidentId)
        {
            try
            {
                var result = DbService.GetActiveEventsById(incidentId, 0, 0);
                var objApiResult = new ApiResult
                {
                    totalCount = result.Count(),
                    pageMetadata = new PageMetaData
                    {
                        content = result.Cast<object>().ToArray(),
                        count = result.Count()
                    }
                };
                return Request.CreateResponse(HttpStatusCode.OK, objApiResult);
            }
            catch (Exception exception)
            {
                ErrorLog.LogError(exception);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, exception.Message);
            }
        }

        [HttpGet, Route("NMSDashboardWS/activeEvents/customercalls/{eventId}")]
        public HttpResponseMessage CustomerCalls(int eventId, int pageno, int limit, string filterby = "")
        {
            try
            {
                var result = DbService.GetActiveEventsById(eventId, pageno, limit);
                var objApiResult = new ApiResult
                {
                    totalCount = result.Count(),
                    totalPages = result.Count > 0 ? (result.FirstOrDefault().TotalCount / limit < limit ? 1 : (int)Math.Ceiling(Convert.ToDecimal(result.FirstOrDefault().TotalCount) / limit)) : 0,
                    pageMetadata = new PageMetaData
                    {
                        content = result.Cast<object>().ToArray(),
                        count = result.Count(),
                        pageNo = pageno
                    }
                };
                return Request.CreateResponse(HttpStatusCode.OK, objApiResult);
            }
            catch (Exception exception)
            {
                ErrorLog.LogError(exception);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, exception.Message);
            }
        }

        [Route("NMSDashboardWS/activeEvents/gettroublecodes/{eventId}")]
        [HttpGet]
        public HttpResponseMessage GetTroubleCodes(int eventId)
        {
            try
            {
                var result = DbService.GetTroubleCodesByEventId(eventId);
                var objApiResult = new ApiResult
                {
                    totalCount = result.Count(),
                    pageMetadata = new PageMetaData
                    {
                        content = result.Cast<object>().ToArray(),
                        count = result.Count()
                    }
                };
                return Request.CreateResponse(HttpStatusCode.OK, objApiResult);
            }
            catch (Exception exception)
            {
                ErrorLog.LogError(exception);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, exception.Message);
            }
        }


        [Route("NMSDashboardWS/activeEvents/incidentdetail/{incidentId}")]
        [HttpGet]
        public HttpResponseMessage incidentdetail(int incidentId)
        {
            try
            {
                var result = DbService.GetIncidentDeatilsById(incidentId);
                var objApiResult = new ApiResult
                {
                    totalCount = result != null ? 1 : 0,
                    pageMetadata = new PageMetaData
                    {
                        content = new object[1] { result },
                    }
                };
                return Request.CreateResponse(HttpStatusCode.OK, objApiResult);
            }
            catch (Exception exception)
            {
                ErrorLog.LogError(exception);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, exception.Message);
            }
        }


        [Route("NMSDashboardWS/activeEvents/customerdetail/{customerId}")]
        [HttpGet]
        public HttpResponseMessage customerdetail(int customerId)
        {
            try
            {
                var result = DbService.GetCustomerDetailsById(customerId);
                var objApiResult = new ApiResult
                {
                    totalCount = result != null ? 1 : 0,
                    pageMetadata = new PageMetaData
                    {
                        content = new object[1] { result },
                    }
                };
                return Request.CreateResponse(HttpStatusCode.OK, objApiResult);
            }
            catch (Exception exception)
            {
                ErrorLog.LogError(exception);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, exception.Message);
            }
        }
    }
}
