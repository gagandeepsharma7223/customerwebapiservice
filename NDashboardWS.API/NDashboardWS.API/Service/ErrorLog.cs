﻿using NDashboardWS.API.Models;
using NDashboardWS.API.Utilities;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace NDashboardWS.API.Service
{
    public class ErrorLog
    {
        public static void LogError(Exception ex, string rootPath = "")
        {
            string message = string.Format("Time: {0}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"));
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;
            message += string.Format("Message: {0}", ex.Message);
            message += Environment.NewLine;
            message += string.Format("StackTrace: {0}", ex.StackTrace);
            message += Environment.NewLine;
            message += string.Format("Source: {0}", ex.Source);
            message += Environment.NewLine;
            message += string.Format("TargetSite: {0}", ex.TargetSite.ToString());
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;
            var now = DateTime.Now;

            string filename = "log_" + now.Day.ToString()+"_" + now.Month.ToString() + "_" + now.Year.ToString() + ".txt";
           
            string folderPath = Path.Combine("ErrorLog");
            string path = string.IsNullOrEmpty(rootPath) ? Path.Combine(HttpContext.Current.Server.MapPath("~/"), folderPath) : Path.Combine(rootPath, folderPath);
            string filepath = Path.Combine(path, filename);

            bool isEmpty = !Directory.EnumerateFiles(path).Any();

            
            if (isEmpty == false)
            {
                var directory = new DirectoryInfo(path);
              var  myFileold = (from f in directory.GetFiles()
                              orderby f.LastWriteTime descending
                              select f).First();



                string filepathold = Path.Combine(path, myFileold.ToString());


                long b = myFileold.Length;
                long kb = b / 1024;
                long mb = kb / 1024;

                if(mb >= 3 )
                {
                    string creFile = path + "/" + filename;

                    var myFile = File.Create(creFile);
                    myFile.Close();
                    using (StreamWriter stwriter = new StreamWriter(creFile, true))
                    {
                        stwriter.WriteLine(message);
                        stwriter.Close();
                    }
                }
                else
                {
                    using (StreamWriter stwriter = new StreamWriter(filepathold, true))
                    {

                        stwriter.WriteLine(message);
                        stwriter.Close();
                    }
                }

                
            }


            if (isEmpty == true)
            {

                if (File.Exists(filepath))
                {


                    using (StreamWriter stwriter = new StreamWriter(filepath, true))
                    {

                        stwriter.WriteLine(message);
                        stwriter.Close();
                    }
                }
                else
                {
                    if (Directory.Exists(path))
                    {

                    }
                    else
                    {
                        Directory.CreateDirectory(path);
                    }
                    string creFile = path + "/" + filename;

                    var myFile = File.Create(creFile);
                    myFile.Close();
                    using (StreamWriter stwriter = new StreamWriter(creFile, true))
                    {
                        stwriter.WriteLine(message);
                        stwriter.Close();
                    }
                }
            }
        }

        public static void LogMessage(string messageText, string rootPath = "")
        {
            string message = string.Format("Time: {0}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"));
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;
            message += string.Format("Message: {0}", messageText);
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;

            //  string pageName = Path.GetFileName(System.Web.HttpContext.Current.Request.Path);
            string filename = "log.txt";
            var now = DateTime.Now;
            string folderPath = Path.Combine("ErrorLog", now.Year.ToString(), now.Month.ToString(), now.Day.ToString());
            string path = string.IsNullOrEmpty(rootPath) ? Path.Combine(HttpContext.Current.Server.MapPath("~/"), folderPath) : Path.Combine(rootPath, folderPath);
            string filepath = Path.Combine(path, filename);
            //string path = System.Web.HttpContext.Current.Server.MapPath("~/ErrorLog/" + DateTime.Now.Year + "/" + DateTime.Now.Month + "/" + DateTime.Now.Day);
            //string filepath = System.Web.HttpContext.Current.Server.MapPath("~/ErrorLog/" + DateTime.Now.Year + "/" + DateTime.Now.Month + "/" + DateTime.Now.Day + "/" + filename);
            if (File.Exists(filepath))
            {
                using (StreamWriter stwriter = new StreamWriter(filepath, true))
                {

                    stwriter.WriteLine(message);
                    stwriter.Close();
                }
            }
            else
            {
                if (Directory.Exists(path))
                {

                }
                else
                {
                    Directory.CreateDirectory(path);
                }
                string creFile = path + "/" + filename;

                var myFile = File.Create(creFile);
                myFile.Close();
                using (StreamWriter stwriter = new StreamWriter(creFile, true))
                {
                    stwriter.WriteLine(message);
                    stwriter.Close();
                }
            }
        }
    }
}