﻿using NDashboardWS.API.Models;
using NDashboardWS.API.Utilities;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;

namespace NDashboardWS.API.Service
{
    public class DbService
    {
        public static IEnumerable<ActiveEvent> GetActiveEvents(int offset, int limit)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            string commandText = "select e.*, crew.crew_name, dmg.assessed, est.description from nms_reporting.active_events e " +
            " left join ( select event_id, array_agg(crew_name) crew_name from nms_reporting.active_events_crew group by event_id) " +
            " crew ON crew.event_id = e.event_id left join (select * from nms_reporting.damage_assessment) dmg on dmg.id = e.dmg_assessment " +
            " left join (select * from nms_reporting.est_source) est on est.source = e.est_source ";
            if (limit > 0)
                commandText = String.Concat(commandText, " OFFSET ", offset, " LIMIT ", limit);

            string query = "select COUNT(*) from nms_reporting.active_events e " +
    " left join ( select event_id, array_agg(crew_name) crew_name from nms_reporting.active_events_crew group by event_id) " +
    " crew ON crew.event_id = e.event_id left join (select * from nms_reporting.damage_assessment) dmg on dmg.id = e.dmg_assessment " +
    " left join (select * from nms_reporting.est_source) est on est.source = e.est_source ";
            int count = GetTotalCount(query);
            DataTable dt = new DataTable("Table1");

            using (NpgsqlConnection connection = new NpgsqlConnection(connectionString))
            {
                connection.Open();
                NpgsqlDataAdapter da = new NpgsqlDataAdapter(commandText, connection);
                da.Fill(dt);
                var result = dt.AsEnumerable().Select(x => new ActiveEvent()
                {
                    status = x["alarm_state"] != DBNull.Value ? x["alarm_state"].ToString() : String.Empty,
                    powerStatus = x["call_type"] != DBNull.Value ? x["call_type"].ToString() : String.Empty,
                    circuit = x["circuit"] != DBNull.Value ? x["circuit"].ToString() : String.Empty,
                    eventId = x["event_id"] != DBNull.Value ? (int)x["event_id"] : 0,
                    numCust = x["num_cust"] != DBNull.Value ? (int)x["num_cust"] : 0,
                    calls = x["calls"] != DBNull.Value ? (int)x["calls"] : 0,
                    eventType = x["event_type"] != DBNull.Value ? (int)x["event_type"] : 0,
                    relationType = x["relation_type"] != DBNull.Value ? (int)x["relation_type"] : 0,
                    dmgAssessment = x["dmg_assessment"] != DBNull.Value ? (int)x["dmg_assessment"] : 0,
                    custCrit = x["cust_crit"] != DBNull.Value ? (int)x["cust_crit"] : 0,
                    custPriority = x["cust_priority"] != DBNull.Value ? (int)x["cust_priority"] : 0,
                    custMed = x["cust_med"] != DBNull.Value ? (int)x["cust_med"] : 0,
                    custKey = x["cust_key"] != DBNull.Value ? (int)x["cust_key"] : 0,
                    wgtCustOut = x["wgt_cust_out"] != DBNull.Value ? (int)x["wgt_cust_out"] : 0,
                    location = x["location"] != DBNull.Value ? x["call_type"].ToString() : String.Empty,
                    device = x["alias"] != DBNull.Value ? x["alias"].ToString() : String.Empty,
                    predictionType = x["prediction_type"] != DBNull.Value ? x["prediction_type"].ToString() : String.Empty,
                    eventTypeDesc = x["event_type_desc"] != DBNull.Value ? x["event_type_desc"].ToString() : String.Empty,
                    division = x["division"] != DBNull.Value ? x["division"].ToString() : String.Empty,
                    estSource = x["est_source"] != DBNull.Value ? x["est_source"].ToString() : String.Empty,
                    etr = x["etr"] != DBNull.Value ? Convert.ToDateTime(x["etr"]) : (DateTime?)null,
                    restoreDate = x["restore_time"] != DBNull.Value ? Convert.ToDateTime(x["restore_time"]) : (DateTime?)null,
                    startDate = x["begin_time"] != DBNull.Value ? Convert.ToDateTime(x["begin_time"]) : (DateTime?)null,
                    clues = x["trouble_code"] != DBNull.Value ? x["trouble_code"].ToString() : String.Empty,
                    dispatchGroup = x["dispatch_group"] != DBNull.Value ? x["dispatch_group"].ToString() : String.Empty,
                    plan = x["plan"] != DBNull.Value ? x["plan"].ToString() : String.Empty,
                    workQueue = x["work_queue"] != DBNull.Value ? x["work_queue"].ToString() : String.Empty,
                    nominalCircuit = x["nominal_circuit"] != DBNull.Value ? x["nominal_circuit"].ToString() : String.Empty,
                    groupType = x["group_type"] != DBNull.Value ? x["group_type"].ToString() : String.Empty,
                    phaseOut = x["phases_out"] != DBNull.Value ? x["phases_out"].ToString() : String.Empty,
                    lastUpdatedBy = x["last_updated_by"] != DBNull.Value ? x["last_updated_by"].ToString() : String.Empty,
                    crewList = x["crew_name"] != DBNull.Value ? x["crew_name"] as string[] : new string[0],
                    description = x["description"] != DBNull.Value ? x["description"].ToString() : String.Empty,
                    substation = x["substation"] != DBNull.Value ? x["substation"].ToString() : String.Empty
                });
                if (result.Count() > 0)
                    result.FirstOrDefault().TotalCount = count;
                return result;
            }
        }

        public static List<ActiveEventCustomerCall> GetActiveEventsById(int eventID, int offset, int limit)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            string commandText = "select event_id, name, phone, address || ',' || addr_city as address,input_time, trouble_code, remarks, premise, meter_id from nms_reporting.active_events_customer_calls where event_id = " + eventID + " order by input_time desc ";
            string query = "select COUNT(*) from nms_reporting.active_events_customer_calls where event_id = " + eventID;
            int count = GetTotalCount(query);
            if (limit > 0)
                commandText = String.Concat(commandText, " OFFSET ", offset, " LIMIT ", limit);
            DataTable dt = new DataTable("Table1");
            using (NpgsqlConnection connection = new NpgsqlConnection(connectionString))
            {
                connection.Open();
                NpgsqlDataAdapter da = new NpgsqlDataAdapter(commandText, connection);
                da.Fill(dt);
                var result = dt.AsEnumerable().Select(x => new ActiveEventCustomerCall()
                {
                    custName = x["name"] != DBNull.Value ? x["name"].ToString() : String.Empty,
                    custPhone = x["phone"] != DBNull.Value ? x["phone"].ToString() : String.Empty,
                    address = x["address"] != DBNull.Value ? x["address"].ToString() : String.Empty,
                    eventId = x["event_id"] != DBNull.Value ? (int)x["event_id"] : 0,
                    callTime = x["input_time"] != DBNull.Value ? Convert.ToDateTime(x["input_time"]) : (DateTime?)null,
                    clues = x["trouble_code"] != DBNull.Value ? x["trouble_code"].ToString() : String.Empty,
                    comments = x["remarks"] != DBNull.Value ? x["remarks"].ToString() : String.Empty,
                    meterId = x["meter_id"] != DBNull.Value ? x["meter_id"].ToString() : String.Empty,
                    clueDesc = x["trouble_code"] != DBNull.Value ? EnumHelper<SiteSafetyCodeEnum>.GetEnumDescription(x["trouble_code"].ToString()) : String.Empty
                }).ToList();
                if (result.Count() > 0)
                    result.FirstOrDefault().TotalCount = count;
                return result;
            }
        }

        public static int GetTotalCount(string query)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            using (NpgsqlConnection connection = new NpgsqlConnection(connectionString))
            {
                using (NpgsqlCommand cmd = new NpgsqlCommand(query, connection))
                {
                    cmd.CommandType = CommandType.Text;
                    connection.Open();
                    var count = cmd.ExecuteScalar().ToString();
                    return Convert.ToInt32(count);
                }
            }
        }

        public static ActiveEventCustomerCall GetActiveEventById(int eventID)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            string commandText = "select event_id, name, phone, address || ',' || addr_city as address,input_time, trouble_code, remarks, premise, meter_id from nms_reporting.active_events_customer_calls where event_id = " + eventID + " order by input_time desc ";
            DataTable dt = new DataTable("Table1");
            using (NpgsqlConnection connection = new NpgsqlConnection(connectionString))
            {
                connection.Open();
                NpgsqlDataAdapter da = new NpgsqlDataAdapter(commandText, connection);
                da.Fill(dt);
                return dt.AsEnumerable().Select(x => new ActiveEventCustomerCall()
                {
                    custName = x["name"] != DBNull.Value ? x["name"].ToString() : String.Empty,
                    custPhone = x["phone"] != DBNull.Value ? x["phone"].ToString() : String.Empty,
                    address = x["address"] != DBNull.Value ? x["address"].ToString() : String.Empty,
                    eventId = x["event_id"] != DBNull.Value ? (int)x["event_id"] : 0,
                    callTime = x["input_time"] != DBNull.Value ? Convert.ToDateTime(x["input_time"]) : (DateTime?)null,
                    clues = x["trouble_code"] != DBNull.Value ? x["trouble_code"].ToString() : String.Empty,
                    comments = x["remarks"] != DBNull.Value ? x["remarks"].ToString() : String.Empty,
                    meterId = x["meter_id"] != DBNull.Value ? x["meter_id"].ToString() : String.Empty,
                    clueDesc = x["trouble_code"] != DBNull.Value ? EnumHelper<SiteSafetyCodeEnum>.GetEnumDescription(x["trouble_code"].ToString()) : String.Empty
                }).FirstOrDefault();
            }
        }

        public static Summary GetSummaryByEventId(int eventID)
        {
            Summary result = new Summary();
            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            string commandText = "select trouble_code from nms_reporting.active_events where event_id= " + eventID;
            string trouble_code = "";
            using (NpgsqlConnection connection = new NpgsqlConnection(connectionString))
            {
                connection.Open();
                using (NpgsqlCommand cmd = new NpgsqlCommand(commandText, connection))
                {
                    trouble_code = Convert.ToString(cmd.ExecuteScalar());
                }

                if (!string.IsNullOrEmpty(trouble_code))
                {
                    var list = trouble_code.Split('-');
                    List<string> safetyCodes = list.Where(x => Enum.GetNames(typeof(SiteSafetyCodeEnum)).Contains(x)).Select(x => x).ToList();
                    List<string> poleCodes = list.Where(x => Enum.GetNames(typeof(PoleGroupCodes)).Contains(x)).Select(x => x).ToList();
                    List<string> treeCodes = list.Where(x => Enum.GetNames(typeof(TreeCodes)).Contains(x)).Select(x => x).ToList();
                    if (safetyCodes.Count > 0)
                    {
                        result.safetyCode = string.Join(",", safetyCodes);
                        result.safety = string.Join(",", safetyCodes.Select(x => EnumHelper<SiteSafetyCodeEnum>.GetEnumDescription(x)));
                    }

                    if (poleCodes.Count > 0)
                    {
                        result.poleCode = string.Join(",", poleCodes);
                        result.pole = string.Join(",", poleCodes.Select(x => EnumHelper<PoleGroupCodes>.GetEnumDescription(x)));
                    }

                    if (treeCodes.Count > 0)
                    {
                        result.treeCode = string.Join(",", treeCodes);
                        result.tree = string.Join(",", treeCodes.Select(x => EnumHelper<TreeCodes>.GetEnumDescription(x)));
                    }
                }
                return result;
            }
        }

        public static List<string> GetTroubleCodesByEventId(int eventID)
        {
            List<string> result = new List<string>();
            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            string commandText = "select trouble_code from nms_reporting.active_events where event_id= " + eventID;
            string trouble_code = "";
            using (NpgsqlConnection connection = new NpgsqlConnection(connectionString))
            {
                connection.Open();
                using (NpgsqlCommand cmd = new NpgsqlCommand(commandText, connection))
                {
                    trouble_code = Convert.ToString(cmd.ExecuteScalar());
                }

                if (!string.IsNullOrEmpty(trouble_code))
                {
                    var list = trouble_code.Split('-');
                    foreach (var item in list)
                    {
                        var desc = string.Empty;
                        if (item.StartsWith("T"))
                        {
                            desc = EnumHelper<TreeCodes>.GetEnumDescription(item);
                            if (desc != string.Empty)
                                result.Add(desc);
                        }
                        else if (item.StartsWith("P"))
                        {
                            desc = EnumHelper<PoleGroupCodes>.GetEnumDescription(item);
                            if (desc != string.Empty)
                                result.Add(desc);
                        }
                        else
                        {
                            desc = EnumHelper<SiteSafetyCodeEnum>.GetEnumDescription(item);
                            if (desc != string.Empty)
                                result.Add(desc);
                        }
                    }
                }
                return result;
            }
        }


        public static IncidentDetail GetIncidentDeatilsById(int eventID)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            string commandText = "select * from nms_reporting.active_events where event_id=" + eventID;
            DataTable dt = new DataTable("Table1");

            using (NpgsqlConnection connection = new NpgsqlConnection(connectionString))
            {
                connection.Open();
                NpgsqlDataAdapter da = new NpgsqlDataAdapter(commandText, connection);
                da.Fill(dt);
                IncidentDetail result = dt.AsEnumerable().Select(x => new IncidentDetail()
                {
                    IncidentStatus = x["alarm_state"] != DBNull.Value ? x["alarm_state"].ToString() : String.Empty,
                    IncidentId = x["event_id"] != DBNull.Value ? (int)x["event_id"] : 0,
                    Address = x["location"] != DBNull.Value ? x["location"].ToString() : String.Empty,
                    Device = x["alias"] != DBNull.Value ? x["alias"].ToString() : String.Empty,
                    Etr = x["etr"] != DBNull.Value ? Convert.ToDateTime(x["etr"]) : (DateTime?)null,
                    StartTime = x["begin_time"] != DBNull.Value ? Convert.ToDateTime(x["begin_time"]) : (DateTime?)null,
                    IncidentType = x["group_type"] != DBNull.Value ? x["group_type"].ToString() : String.Empty,
                    TroubleCodes = x["trouble_code"] != DBNull.Value ? x["trouble_code"].ToString() : String.Empty
                }).FirstOrDefault();

                if (!string.IsNullOrEmpty(result.TroubleCodes))
                {
                    var troubleCodeList = new List<string>();
                    var list = result.TroubleCodes.Split('-');
                    foreach (var item in list)
                    {
                        var desc = string.Empty;
                        if (item.StartsWith("T"))
                        {
                            desc = EnumHelper<TreeCodes>.GetEnumDescription(item);
                            if (desc != string.Empty)
                                troubleCodeList.Add(desc);
                        }
                        else if (item.StartsWith("P"))
                        {
                            desc = EnumHelper<PoleGroupCodes>.GetEnumDescription(item);
                            if (desc != string.Empty)
                                troubleCodeList.Add(desc);
                        }
                        else
                        {
                            desc = EnumHelper<SiteSafetyCodeEnum>.GetEnumDescription(item);
                            if (desc != string.Empty)
                                troubleCodeList.Add(desc);
                        }
                    }

                    result.TroubleCodes = string.Join(",", troubleCodeList);
                }

                return result;
            }
        }

        public static CustomerDetails GetCustomerDetailsById(int CoustomerID)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            string commandText = "select e.*, c.circuit  from nms_reporting.active_events_customer_calls e left join nms_reporting.active_events c on e.event_id = c.event_id where e.event_id = " + CoustomerID;
            DataTable dt = new DataTable("Table1");

            using (NpgsqlConnection connection = new NpgsqlConnection(connectionString))
            {
                connection.Open();
                NpgsqlDataAdapter da = new NpgsqlDataAdapter(commandText, connection);
                da.Fill(dt);
                CustomerDetails result = dt.AsEnumerable().Select(x => new CustomerDetails()
                {
                    Address = x["address"] != DBNull.Value ? x["address"].ToString() : String.Empty,
                    BillAcct = x["account_number"] != DBNull.Value ? x["account_number"].ToString() : String.Empty,
                    Circuit = x["circuit"] != DBNull.Value ? x["circuit"].ToString() : String.Empty,
                    Phone = x["phone"] != DBNull.Value ? x["phone"].ToString() : String.Empty,
                    City = x["addr_city"] != DBNull.Value ? x["addr_city"].ToString() : String.Empty

                }).FirstOrDefault();
                return result;
            }
        }
    }
}