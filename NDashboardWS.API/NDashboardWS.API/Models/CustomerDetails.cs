﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NDashboardWS.API.Models
{
    public class CustomerDetails
    {
        public string BillAcct { get; set; }

        public string Name { get; set; }

        public string Circuit { get; set; }

        public string Phone { get; set; }

        public string IncidentStatus { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string Zip { get; set; }

        public string Address { get; set; }

        public string Lse { get; set; }
    }
}