﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NDashboardWS.API.Models
{
    public class ActiveEvent
    {
        public string status { get; set; }
        public int eventId { get; set; }
        public string powerStatus { get; set; }
        public string circuit { get; set; }
        public int numCust { get; set; }
        public string location { get; set; }
        public string clues { get; set; }
        public string device { get; set; }
        public string crew
        {
            get
            {
                return crewList != null ? crewList.FirstOrDefault() : String.Empty;
            }
        }
        public string[] crewList { get; set; }
        public string predictionType { get; set; }
        public int calls { get; set; }
        public int eventType { get; set; }
        public string eventTypeDesc { get; set; }
        public string division { get; set; }
        public DateTimeOffset? etr { get; set; }
        public DateTimeOffset? restoreDate { get; set; }
        public string estSource { get; set; }
        public int relationType { get; set; }
        public string dispatchGroup { get; set; }
        public string plan { get; set; }
        public string workQueue { get; set; }
        public string nominalCircuit { get; set; }
        public int dmgAssessment { get; set; }
        public int custCrit { get; set; }
        public int custPriority { get; set; }
        public int custMed { get; set; }
        public int custKey { get; set; }
        public string substation { get; set; }
        public string groupType { get; set; }
        public string phaseOut { get; set; }
        public int wgtCustOut { get; set; }
        public DateTimeOffset? startDate { get; set; }
        public string lastUpdatedBy { get; set; }
        public string description { get; set; }
        public int TotalCount { get; set; }
        public ActiveEvent()
        {
        }
    }
}