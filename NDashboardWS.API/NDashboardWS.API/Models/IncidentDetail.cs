﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NDashboardWS.API.Models
{
    public class IncidentDetail
    {
        public Int64 IncidentId { get; set; }

        public DateTime? StartTime { get; set; }

        public string Lse { get; set; }

        public Int64 NumCustAffected { get; set; }

        public string IncidentType { get; set; }

        public string IncidentStatus { get; set; }

        public string Cause { get; set; }

        public string Device { get; set; }

        public string TroubleCodes { get; set; }

        public string Phone { get; set; }

        public string Address { get; set; }

        public string City { get; set; }

        public DateTime? Etr { get; set; }

        public string Restored { get; set; }

        public string Duration { get; set; }

        public string Calls { get; set; }

        public string Div { get; set; }

        public string DivName { get; set; }

        public string StatusDesc { get; set; }
    }
}