﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NDashboardWS.API.Models
{
public class Summary
   {
       public string safety { get; set; }
       public string tree { get; set; }
       public string pole { get; set; }
       public string safetyCode { get; set; }
       public string treeCode { get; set; }
       public string poleCode { get; set; }
   }
}
